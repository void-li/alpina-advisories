#!/bin/bash
# Copyright © 2021 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#
set -e -x

# ----------

images=()

while [ "$#" -gt 0 ]; do
  if [ -n "$1" ]; then
    images+=("$1");
  fi
  shift
done

# ----------

cleanup_tmp_dir() {
  if [ -d "$tmp_dir" ]; then
    rm -rf "$tmp_dir"
  fi
}

trap cleanup_tmp_dir EXIT INT TERM

tmp_dir=$(mktemp -d)

# ----------

for image in "${images[@]}"; do
  IFS=':' read project_pid project_ref package_lst <<< "$image"

  if [ -f "$package_lst" ] && [ -n "$(cat "$package_lst")" ]; then
    touch "$tmp_dir/$project_pid"
  fi
done

# ----------

for image in "${images[@]}"; do
  IFS=':' read project_pid project_ref package_lst <<< "$image"

  if [ -f "$tmp_dir/$project_pid" ]; then
    rm -f "$tmp_dir/$project_pid"

    curl -X POST -d "token=$CI_JOB_TOKEN" -d "ref=$project_ref" \
      "https://gitlab.com/api/v4/projects/$project_pid/trigger/pipeline"
  fi
done
